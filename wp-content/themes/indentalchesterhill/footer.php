        </div> <!-- end div content -->
      </div> <!-- end div main container -->
      <?php
        the_module('footer',
          array(
            'copyright' => get_field('footer_copyright', 'option'),
            'logo' => get_field('footer_logo', 'option'),
            'introduction' => get_field('footer_introduction', 'option'),
            'contact_label' => get_field('footer_contact_label', 'option'),
            'contacts' => get_field('footer_contact', 'option'),
            'app_download_label' => get_field('footer_app_download_label', 'option'),
            'app_download_ios' => get_field('footer_app_download_ios', 'option'),
            'app_download_android' => get_field('footer_app_download_android', 'option'),
            'socials' => get_field('social_media_links', 'option')
          )
        );
      ?>
    </div><!-- /#page -->
    <?php
      $form_id = get_field('footer_sticky_form_id', 'option');
      the_module('footer-scroll-top',
        array(
          'title' => get_field('footer_sticky_title', 'option'),
          'button_label' => get_field('footer_sticky_button', 'option'),
          'form_id' => (!empty($form_id)) ? $form_id : 238,
        )
      );
    ?>
    <?php the_module('footer-popup'); ?>
    <?php wp_footer(); ?>
  </body>
</html>
