<?php get_header(); ?>
<article class="bg-merino page">
  <?php
  while (have_posts()) :
    the_post();

    $title = get_the_title();
    $background_color = get_field('hero_background_color');
    // Default class
    $hero_class = 'hero-title--basic-page hero-title--page-default';
    $content_class = 'content--default';

    $enable_sidebar = get_field('enable_sidebar');
    $sidebar_content = get_field('sidebar_content');
    if($background_color == 'orange') {
      $hero_class .= ' hero-title--orange-background';
    }

    if($enable_sidebar && !empty($sidebar_content)) {
      $content_class = 'content--with-sidebar';
    }

    the_module('hero-title', array(
      'title' => get_field('hero_headline'),
      'headline' => $title,
      'description' => get_field('hero_description'),
      'class' => $hero_class
    ));

    the_module('content', array(
      'class' => $content_class,
      'enable_sidebar' => $enable_sidebar,
      'sidebar_content' => get_field('sidebar_content'),
      'title' => $title
    ));

  endwhile;
  ?>
</article>
<?php get_footer(); ?>
