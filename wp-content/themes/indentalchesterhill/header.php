<!doctype html>
<html class="mouse lower modern chrome">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Open graph tags -->
    <meta property="og:site_name" content="Cindi Site">
    <meta property="og:title" content="Cindi Site">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://localhost/ab_html/">
    <meta property="og:description" content="">

    <!-- *** wordpress online css *** -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/css/font-awesome.css" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Baloo+Tammudu|Montserrat:100,300,400,400i,500,500i,600,600i,700,700i&display=swap&subset=telugu,vietnamese" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="chrome-extension://limmgihpbambgjgfbdeannfbfcfpodfk/css/main.css">
    <!-- *** end wordpress online css *** -->

    <!-- *** wordpress head default *** -->
    <?php wp_head(); ?>
    <!-- *** end wordpress head default *** -->
  </head>
  <body cz-shortcut-listen="true" <?php body_class(); ?>>
    <div id="cindi_landing">
      <!-- header fixed icon -->
      <?php the_module('header-fixed-icon'); ?>
      <div id="page">
        <!-- header menu -->
        <?php
          $menu_locations = get_nav_menu_locations();
          $menu_id = $menu_locations['header-primary'];
          $menu_items = wp_get_nav_menu_items($menu_id);
          the_module('header', array(
            'logo' => get_field('logo', 'option'),
            'menu' => $menu_items,
          )
        ); ?>

        <div class="main-container" id="main">
          <div id="content">
