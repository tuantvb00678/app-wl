<?php if ( !empty($list) || !empty($title) ) : ?>
<!-- start secsion Teacher -->
<div class="con_list_teacher" id="divteacher">
  <div class="container">
    <h3 class="st_title"><span><?php echo $title; ?></span></h3>
    <?php if ( !empty($list) ): ?>
    <?php $teacher_list = array_chunk($list, 4); ?>
    <div class="box_list_teacher">
      <?php foreach($teacher_list as $teachers): ?>
        <div class="box_item">
          <div class="row">
            <?php foreach($teachers as $teacher): ?>
              <div class="col-xs-12 col-sm-6">
                <div class="box_teacher">
                  <p class="photo">
                    <img src="<?php echo $teacher['image']; ?>" alt="<?php echo $teacher['title']; ?>" />
                  </p>
                  <div class="box_intro">
                    <h3><?php echo $teacher['title']; ?>
                      <span><?php echo $teacher['score']; ?></span>
                    </h3>
                    <p class="box-txt"><?php echo $teacher['content']; ?></p>
                    <div class="our-reviewer-item">
                      <div class="star-ratings-sprite">
                        <span style="width:<?php echo (empty($teacher['width'])) ? '100%' : $teacher['width'];?>%" class="star-ratings-sprite-rating"></span>
                      </div>
                      <h6><?php echo $teacher['ratting']; ?></h6>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach;?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <?php if( !empty($button_label) ) : ?>
      <div class="box_btn text-center">
        <?php $url = (!empty($button_link)) ? $button_link : '#';?>
        <a class="btn btn-lg" href="<?php echo $url; ?>"><?php echo $button_label; ?></a>
      </div>
    <?php endif; ?>
  </div>
</div>
<!-- end secsion Teacher -->
<?php endif; ?>
