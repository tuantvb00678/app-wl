<?php if ( !empty($list) || !empty($title) ): ?>
<!-- start secsion reason -->
<div class="con_reason">
  <div class="container">
    <h3 class="st_title"><span><?php echo $title; ?></span></h3>
    <?php if ( !empty($list) ) : ?>
      <div class="box_reason">
        <ul>
          <?php foreach($list as $item): ?>
          <li>
            <p class="photo">
              <img src="<?php echo $item['image']; ?>" alt="<?php echo $item['title']; ?>" />
            </p>
            <h4><?php echo $item['title']; ?></h4>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php endif; ?>
  </div>
</div>
<!-- end secsion reason -->
<?php endif; ?>
