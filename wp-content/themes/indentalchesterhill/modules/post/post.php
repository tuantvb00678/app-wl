<div class="post-content">
  <article class="post post--article">
    <div class="con_list_news">
      <div class="container">
        <div class="st_title">
          <span><?php the_title(); ?></span>
        </div>
        <div class="content">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </article>
</div>
