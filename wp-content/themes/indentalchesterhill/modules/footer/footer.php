<div id="footer">
  <div class="con_footer">
    <div class="container">
      <div class="box_footer">
        <div class="scroll_top_mobile  hidden-md hidden-lg hidden-sm">
          <a href="#main">
            <i class="fas fa-arrow-up"></i>
          </a>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-5">
            <div class="footer-introduction">
              <?php if( isset($logo['url']) ): ?>
                <img src="<?php echo $logo['url']; ?>" alt="">
              <?php endif; ?>
              <div class="info"><?php echo $introduction; ?></div>
              <!-- <h4>FOLLOW US</h4> -->
              <?php if( !empty($socials) ): ?>
                <?php $icons = array('fab fa-facebook-f', 'fab fa-twitter', 'fab fa-instagram'); ?>
                <div class="footer-social-group">
                  <?php foreach($socials as $key => $social): ?>
                    <div class="footer-social-item"><a href="<?php echo $social['url']; ?>"><i class="<?php echo $icons[$key%3]; ?>"></i></a></div>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-5">
            <div class="footer-contact">
              <h4><?php echo $contact_label; ?></h4>
              <?php if( !empty($contacts) ): ?>
                <?php $icons = array('fas fa-map-marker-alt', 'fas fa-phone-volume', 'fa fa-envelope'); ?>
                <?php foreach($contacts as $key=>$contact): ?>
                  <div class="footer-contact-item">
                    <div class="detail-contact">
                      <div class="contact"><i class="<?php echo $icons[$key%3]; ?>"></i> <?php echo $contact['contact_item']; ?></div>
                    </div>
                  </div>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          </div>
          <?php if( !empty($app_download_label) ): ?>
            <div class="col-xs-12 col-sm-2">
              <h4><?php echo $app_download_label; ?></h4>
              <div class="box_lnk">
                <?php
                  $app_download_ios_img = THEME_URI . "/assets/images/img_appstore.png";
                  $app_download_android_img = THEME_URI . "/assets/images/img_googleplay.png";
                ?>
                <a href="<?php echo $app_download_ios; ?>"><img src="<?php echo $app_download_ios_img; ?>" alt="" /></a>
                <a href="<?php echo $app_download_android; ?>"><img src="<?php echo $app_download_android_img; ?>" alt="" /></a>
              </div>
            </div>
          <?php endif; ?>

        </div>

      </div>
    </div>

    <?php if( !empty($copyright) ): ?>
      <p class="copyright"><?php echo $copyright; ?></p>
    <?php endif; ?>
  </div>
</div><!-- /#footer -->
