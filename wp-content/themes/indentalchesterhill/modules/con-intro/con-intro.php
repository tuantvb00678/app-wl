<?php if ( !empty($youtube_link) || !empty($title) || !empty($sub_title)) : ?>
<div class="con_intro" id="divabout">
  <div class="container">
    <div class="box_intro">
      <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
          <p class="photo_intro">
            <iframe width="1140" height="600" src="<?php echo $youtube_link; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <!-- <img src="http://lndentalchesterhill:8888/wp-content/themes/indentalchesterhill/assets/images/img_intro1.png" alt="" /> -->
          </p>
          <div class="box_info">
            <h3>
              <span><?php echo $title; ?></span>
              <?php echo $sub_title; ?>
            </h3>
            <div class="box-txt"><?php echo $content; ?></div>
            <!-- <div class="box_sign">
              <p class="photo"><img src="http://lndentalchesterhill:8888/wp-content/themes/indentalchesterhill/assets/images/img_signature.png" alt="" /></p>
              <h5>Tung Tony <span>- Giám đốc trung tâm -</span></h5>
            </div>
            <h6>Tư Vấn Hỗ Trợ <span>0909 469 666</span></h6>
            <a class="btn btn-primary">Liên Hệ</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
