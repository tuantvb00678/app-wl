<div class="scroll_top visible-md visible-lg visible-sm">
  <a href="#main">
    <i class="fas fa-arrow-up"></i>
  </a>
</div>
<div class="con_present" id="present">
  <div class="box_present">
    <div class="box_info">
      <?php if ( !empty($title) ): ?>
        <h4><?php echo $title; ?></h4>
      <?php endif; ?>
      <div class="con_present-form js-form-phone">
        <?php if ( !empty($form_id) ) :
          echo do_shortcode( '[contact-form-7 id="'.$form_id.'"]' );
        endif; ?>
        <!-- <form action="/action_page.php" >
          <div class="form-group">
            <input class="form-control" type="text" placeholder="Nhập số điện thoại" name="name" required>
            <button type="submit" class="btn btn-lg"><?php echo $button_label; ?></button>
          </div>
        </form> -->
      </div>
    </div>
    <?php $image_url = THEME_URI . "/assets/images/img_present.png";?>
    <p class="photo"><img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" /></p>
  </div>
</div>
