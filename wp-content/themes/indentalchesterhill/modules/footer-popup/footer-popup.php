<script type="text/javascript">
  window.onscroll = function() {myFunction()};

  var present = document.getElementById("present");
  // var sticky = present.offsetTop;
  var widthContent = document.getElementById("main").offsetHeight;

  function myFunction() {
    if (window.pageYOffset > 200 && window.pageYOffset < widthContent) {
      present.classList.add("sticky");
    } else {
      present.classList.remove("sticky");
    }
  }
</script>
<div id="lbdictex_find_popup" class="lbexpopup hidden" style="position: absolute; top: 0px; left: 0px;">
  <div class="lbexpopup_top"><h2 class="fl popup_title">&nbsp;</h2>
    <ul>
      <li><a class="close_main popup_close" href="#">&nbsp;</a></li>
    </ul>
    <div class="clr"></div>
  </div>
  <div class="popup_details"></div>
  <div class="popup_powered">abc</div>
</div>
<div id="lbdictex_ask_mark" class="hidden" style="position: absolute; top: 0px; left: 0px;">
  <a class="lbdictex_ask_select" href="#">&nbsp;</a>
</div>
