<?php if ( !empty($title) || !empty($list) ): ?>
<!-- start secsion course -->
<div class="con_course">
  <div class="container">
    <h3 class="st_title"><span><?php echo $title; ?></span></h3>
    <?php if ( !empty($list) ): ?>
    <div class="box_course">
      <ul>
        <?php foreach($list as $item): ?>
        <li>
          <p class="photo">
            <img src="<?php echo $item['image']; ?>" alt="" />
            <span class="num1"><img src="<?php echo $item['number']; ?>" alt="" /></span>
          </p>
          <h4><?php echo $item['title']; ?></h4>
          <div><?php echo $item['content']; ?></div>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
    <?php endif; ?>
  </div>
</div>
<!-- end secsion Course -->
<?php endif; ?>
