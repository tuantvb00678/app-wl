<?php
add_filter('gform_confirmation_anchor', '__return_true');
add_filter('gform_field_css_class', 'gform_custom_class', 10, 3);

function gform_custom_class($classes, $field, $form)
{
  $unformat_field_types = array('checkbox', 'radio');
  if (!in_array($field->type, $unformat_field_types)) {
    $classes .= ' js-field';
  }
  $classes .= ' gfield--' . $field->type;
  return $classes;
}

add_filter('gform_form_tag', 'gform_custom_attribute', 10, 2);
function gform_custom_attribute($form_tag, $form)
{
  $form_tag = preg_replace('/(<form\b[^><]*)>/i', '$1 data-module="gravity-form">', $form_tag);
  return $form_tag;
}

add_filter('gform_submit_button', 'form_submit_button', 10, 2);
function form_submit_button($button, $form)
{
  if ($form) {
    $value = $form['button']['text'];
    $classButton = 'button-secondary';
    if ($form['id'] == get_field('newsletter_form_id', 'options')) {
      $classButton = 'button-tertiary button gform_button';
    }
    if ($form['title'] == 'Contact' || $form['title'] == 'Application') {
      $classButton = 'button';
    }
    return '<button id="gform_submit_button_' . $form['id'] . '" class="' . $classButton . ' button-animate" data-module="button"><span class="button__text">' . $value . '</span><canvas width="162" height="100"></canvas></button>';
  }
  return $button;
}

// Change default tabindex from -1 to 0
add_filter('gform_tabindex', function ($tabindex, $form) {
  return 0;
}, 10, 2);

if ( !is_admin() ) {
  add_filter( 'gform_field_content', function( $field_content, $field ) {
    if ( $field->cssClass == 'js-fields-card' ) {
      $inputs = $field->inputs;
      if (!empty($inputs)) {
        $input_id = 'input_' . $inputs[0]['id'];
        $label = $inputs[0]['label'];
        $field_content = str_replace( "<input type='text' name='".$input_id."'", '<div class="gravity-input-field"><input type="text" name="'.$input_id.'"', $field_content );
        $field_content = str_replace( $label."</label>", $label.'</label></div>', $field_content );
      }
      $arrow = _get_svg('arrow');
      $field_content = str_replace( '<select', '<div class="gravity-select-field"><select', $field_content );
      $field_content = str_replace( '</select>', '</select>' . $arrow . '</div>', $field_content );
      $field_content = str_replace( "<span class='ginput_card_security_code_icon'>&nbsp;</span>", '', $field_content );
    }
    return $field_content;
  }, 10, 2 );

  add_filter('gform_field_container', function ($field_container, $field, $form, $css_class, $style, $field_content) {
    if (strpos($field->cssClass, 'gfield-group-start') !== false) {
      $field_container = str_replace("<li", '<li class="gfield gfield-group"><ul class="gfield-group__wraper"><li', $field_container);
    }
    if (strpos($field->cssClass, 'gfield-group-end') !== false) {
      $field_container = str_replace("</li>", '</li></ul></li>', $field_container);
    }
    return $field_container;
  }, 10, 6);
}
