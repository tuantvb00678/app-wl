<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lndentalchesterhill');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#6=~{BqzZ`6H rj}F>VWgG>1Br|!KIuRT/7W~*@5wb$|p`g@Z_VLt&](V4ZZ)47]');
define('SECURE_AUTH_KEY',  'Zs=7(>ic/K:V{Ftc91Sk*VuYig.3,!l3o3,DBwe+bA_JB>#zTcjr@%eXK$q^*LWw');
define('LOGGED_IN_KEY',    'CR7R6h+@inQR )nRr X}#%K1 j=TbrW+UV/;3)J.|@an0+MLS>ksGQb/(T/sY?XX');
define('NONCE_KEY',        'kM|XQa:ZRkr)pcn8tt~,8g9zTn)LJ={ss3!jLE7G.Sv=ITT6s)JCnS[lTbOP|>Ee');
define('AUTH_SALT',        'gx#ars)J1ZJ5py;!#zFyIn:h*4tlY+fiAUZON{W#Zp7qI,^/]t6@@2tI?)Dr]Uto');
define('SECURE_AUTH_SALT', 'x./wC}4%Nj}@;Izw|i]!Q1~}/nEj3~Y&E7@imSm]-@Aml@.n*A Xd.exle<|A{28');
define('LOGGED_IN_SALT',   '^C)@8{d:{;BkE[@ee9c,|Bx=Y;)),U4}/Xl+&;;QVGW,ujRAT|X2rB^T+(DB^T:U');
define('NONCE_SALT',       '1@P{Nsa))eQyBw#NZ[2YX9f-ehY[2am6k&ffi$JLgK>3G- 1(5{5A3I@X(}S.H}8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
